﻿using CsvHelper;
using Fun.Core;
using Fun.Service;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Fun.Main
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddScoped<IPartnerService, PartnerService>()
                .AddSingleton<IKoopManager, KoopManager>()
                .AddTransient<IRankLogic, RankLogic>()
                .BuildServiceProvider();

            var koopManager = serviceProvider.GetService<IKoopManager>();

            await GetTop10Results(koopManager, "Top10Makelaars.csv", "Makelaars", "Amsterdam");
            
            await GetTop10Results(koopManager, "Top10MakelaarsWithTuin.csv", "Makelaars With Tuin", "Amsterdam", "Tuin");

            Console.ReadKey();
        }

        private static async Task GetTop10Results(IKoopManager koopManager, string fileName, string listName, params string[] searchTerms)
        {
            try
            {
                var top10Makelaars = await koopManager.GetTopMakelaars(10, searchTerms);
                ShowTopListOnConsole(top10Makelaars, listName);
                WriteToCsv(top10Makelaars, fileName);
                Console.Beep();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong with getting results");
                Console.WriteLine(ex.Message);
            }
        }

        private static void WriteToCsv(IEnumerable<Makelaar> result, string fileName)
        {
            using (var writer = new StreamWriter(fileName))
            {
                using (var csv = new CsvWriter(writer))
                {
                    csv.WriteRecords(result.Select(r => new 
                    { 
                        r.Id, 
                        r.Name, 
                        NumberOfListing = r.Objects.Count() 
                    }));
                }
            }
        }

        private static void ShowTopListOnConsole(IEnumerable<Makelaar> result, string listName)
        {
            Console.WriteLine($"TOP 10 LIST - {listName}");
            var rank = 0;
            foreach (var item in result.ToList())
            {
                rank++;
                Console.WriteLine($"Rank: {rank}");
                Console.WriteLine($"Makelaar Name: {item.Name}, Id: {item.Id}");
                Console.WriteLine($"Number of objects listed: {item.Objects.Count()}");
            }
            Console.WriteLine("#------#------#------#------#------#------#------#------#");

        }
    }
}
