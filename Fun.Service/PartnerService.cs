﻿using Fun.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Fun.Service
{
    public class PartnerService : IPartnerService
    {
        private readonly HttpClient httpClient;
        private const string MainUri = "http://partnerapi.funda.nl/feeds/Aanbod.svc/json/ac1b0b1572524640a0ecc54de453ea9f";

        public PartnerService()
        {
            httpClient = new HttpClient();
        }
        public async Task<List<KoopObject>> GetKoopObjects(PartnerServiceParameter serviceParameter)
        {
            List<KoopObject> koopObjects = new List<KoopObject>();

            var url = MakeUri(serviceParameter);
            var page = 1;
            var pageSize = 25;

            var timeDelay = TimeSpan.FromSeconds(60);

            while (true)
            {
                var serviceUrl = $"{url}&page={page}&pagesize={pageSize}";

                try
                {
                    var httpResponse = await httpClient.GetAsync(new Uri(serviceUrl));
                    if (httpResponse.IsSuccessStatusCode)
                    {
                        var responseString = await httpResponse.Content.ReadAsStringAsync();
                        var koopResults = JsonSerializer.Deserialize<KoopResult>(responseString);

                        if (koopResults.Objects.Count() == 0)
                        {
                            break;
                        }

                        page++;

                        koopObjects.AddRange(Map(koopResults));
                    }
                    else if((int)httpResponse.StatusCode == 429 || 
                        ((int)httpResponse.StatusCode == 401 && httpResponse.ReasonPhrase.Contains("exceeded")))
                    {
                        //Wait for a minute
                        await Task.Delay(timeDelay);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return koopObjects;
        }

        private string MakeUri(PartnerServiceParameter serviceParameter)
        {
            StringBuilder searchQueryParam = new StringBuilder("&zo=/");
            foreach (var searchTerm in serviceParameter.SearchTerms)
            {
                searchQueryParam.Append(searchTerm);
                searchQueryParam.Append("/");
            }
            return $"{MainUri}/?type={serviceParameter.Type}{searchQueryParam.ToString()}";
        }

        private IEnumerable<KoopObject> Map(KoopResult koopResults)
        {
            var koopObjects = koopResults.Objects.Select(koopResultObject => new KoopObject
            {
                Address = koopResultObject.Adres,
                Id = Guid.Parse(koopResultObject.Id),
                Makelaar = new Makelaar
                {
                    Id = koopResultObject.MakelaarId,
                    Name = koopResultObject.MakelaarNaam
                },
                PostCode = koopResultObject.Postcode,
                WithTuin = false
            });

            return koopObjects;
        }
    }
}
