﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fun.Core
{
    public interface IPartnerService
    {
        Task<List<KoopObject>> GetKoopObjects(PartnerServiceParameter serviceParameter);
    }
}
