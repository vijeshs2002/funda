﻿using System.Collections.Generic;
using System.Linq;

namespace Fun.Core
{
    public class RankLogic : IRankLogic
    {
        public IEnumerable<Makelaar> GetTopMakelaarsWithMostObjects(List<KoopObject> objects, int topNumber)
        {
            var makelaarIds = objects.Select(o => o.Makelaar.Id).Distinct();

            var makelaars1 = new List<Makelaar>();

            foreach (var makelaarId in makelaarIds)
            {
                var makelaarObjects = objects.Where(o => o.Makelaar.Id == makelaarId);
                var mak = new Makelaar() { Id = makelaarId, Name = makelaarObjects.FirstOrDefault().Makelaar.Name };
                mak.Objects = makelaarObjects;
                makelaars1.Add(mak);
            }

            makelaars1.Sort(
                (m1, m2) =>
            m2.Objects.Count().CompareTo(m1.Objects.Count()));
            return makelaars1.Take(topNumber);
        }
    }
}
