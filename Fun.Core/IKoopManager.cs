﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fun.Core
{
    public interface IKoopManager
    {
        Task<IEnumerable<Makelaar>> GetTopMakelaars(int number, params string[] searchTerms);
    }
}
