﻿using System.Collections.Generic;

namespace Fun.Core
{
    public class PartnerServiceParameter
    {
        public string Type { get; set; }
        public List<string> SearchTerms { get; }
        public PartnerServiceParameter()
        {
            Type = string.Empty;
            SearchTerms = new List<string>();
        }
    }
}
