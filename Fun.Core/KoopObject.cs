﻿using System;

namespace Fun.Core
{
    public class KoopObject
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
        public bool WithTuin { get; set; }
        public Makelaar Makelaar { get; set; }
        public string PostCode { get; set; }

    }
}
