﻿using System.Collections.Generic;

namespace Fun.Core
{
    public interface IRankLogic
    {
        IEnumerable<Makelaar> GetTopMakelaarsWithMostObjects(List<KoopObject> objects, int topNumber);
    }
}
