﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fun.Core
{
    public class KoopManager : IKoopManager
    {
        private readonly IPartnerService partnerService;
        private readonly IRankLogic rankLogic;

        public KoopManager(IPartnerService partnerService, IRankLogic rankLogic)
        {
            this.partnerService = partnerService ?? throw new ArgumentNullException(nameof(partnerService));
            this.rankLogic = rankLogic ?? throw new ArgumentNullException(nameof(rankLogic));
        }

        public async Task<IEnumerable<Makelaar>> GetTopMakelaars(int number, params string[] searchTerms)
        {
            List<KoopObject> objects = await MakeServiceCall(searchTerms);

            return rankLogic.GetTopMakelaarsWithMostObjects(objects, number);
        }

        private Task<List<KoopObject>> MakeServiceCall(params string[] searchTerms)
        {
            var parameters = new PartnerServiceParameter()
            {
                Type = "koop"
            };

            parameters.SearchTerms.AddRange(searchTerms);

            return partnerService.GetKoopObjects(parameters);
        }
    }
}
