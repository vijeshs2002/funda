﻿using System.Collections.Generic;

namespace Fun.Core
{
    public class Makelaar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<KoopObject> Objects { get; set; }
    }
}
